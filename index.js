





fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(json => {
    let titles = json.map(users => (`${users.title}`));
    console.log(titles);
 });


fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'delectus aut autem', 
		completed: false,
		id: 1,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To do List Items', 
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To do List Items', 
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
// .then((json) => console.log(json));




fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		dateCompleted: "Pending",
		description: "To update my to do list with a different date structure",
		status: "Pending",
		title: "Updated to do list items"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: "delectus aut autem"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});



const express = require("express");

//  app - Server
const app = express();

const port = 3000;

// Middlewares - software that provides commons services and capabilities to applications outside of what's offered by the operating system 

// allows your app to read JSON data
app.use(express.json());

app.listen(port, () => console.log(`Server running at ${port}`));
